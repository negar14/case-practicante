**Para ejecutar localmente el front-end:**

*Desde front-end/aplicativo/*

npm install

npm start

**Para ejecutar localmente el back-end:**

*Desde back-end/servidor/*

npm install

npm start
**=======================================================================================================================================================**
*NOTA: El aplicativo que desarrolles será completamente tu propiedad, la única intención es evaluar tus capacidades y no se utilizará ningún elemento de tu código sin tu permiso. Asimismo, no estás autorizado de utilizar la data de esta evaluación fuera de la misma.*

La siguiente tarea busca simular una parte de un proyecto típico del desarrollo de productos en el CAA.

**Se necesita un aplicativo web desarrollado en JavaScript**. Puedes utilizar todas las bibliotecas que desees, la tecnología de front-end y back-end que gustes, pero mientras más sencillo, mejor. ¡Solo no olvides de usar JavaScript!

El archivo `products.json` contiene un array de objetos, cada uno representa un producto específico que vende una empresa.

El objetivo del aplicativo es poder hallar fácilmente un producto en función a sus características:

- Categoría (`DE_CATE`)
- Marca (`DE_EQUI`)
- Familia (`DE_FAMI`)

*NOTA: Las características están (casi) en orden de granularidad. Es decir, una categoría tiene un conjunto marcas, y una marca tiene un conjunto de familias. Sin embargo, una marca puede pertenecer a más de una categoría. En la práctica, uno eligiría primero la categoría, luego la marca y luego la familia. El aplicativo deberá reflejar esto*.

**El array de productos deberá estar alojado en un RESTful API (local o externo) y el aplicativo deberá consumir este API.**

La idea es crear alguna forma amigable de filtrar los productos para luego obtener una lista de resultados.

Por ejemplo, que el usuario seleccione una categoría, luego una marca, y luego una familia y que se muestren en otra pantalla los resultados de los productos que cumplan con esas características.

Clona este repositorio localmente y **desarrolla el API y el aplicativo dentro de la carpeta `case-practicante`**.

Se evaluará la calidad del código, la eficiencia de la lógica, la presentación del producto (interfaz de usuario, diseño), y el tiempo que te demore hacerlo.

Cuando termines puedes enviarme tu trabajo a [jnaranjo@estrategica.com.pe](mailto:jnaranjo@estrategica.com.pe). También puedes contactarme por ahí si es que tienes alguna consulta.

¡Mucha suerte!  
~ Jose Naranjo