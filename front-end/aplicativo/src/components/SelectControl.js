import React from 'react';
import {FormGroup,ControlLabel,FormControl} from 'react-bootstrap';

const SelectControl = function(props) {

  return (
    <FormGroup controlId={props.controlId}>
      <ControlLabel>{props.title}</ControlLabel>
      <FormControl disabled={props.disabled}
                   value={props.value}
                   componentClass="select"
                   onChange={props.handleChange} >
        <option value="empty">{props.defaultOption}</option>
        {props.list.map((item,i) => {
          return <option key={i} value={item}>{item}</option>
        })}
      </FormControl>
    </FormGroup>
  );

};

export default SelectControl;
