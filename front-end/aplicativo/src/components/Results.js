import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import {Redirect} from 'react-router-dom';
import {Button} from 'react-bootstrap';
import paginationFactory from 'react-bootstrap-table2-paginator';


class Results extends Component{

  render(){
    const columns = [{ //Product table's columns
        dataField: 'ID_ITEM',
        text: 'Code'
      },
      {
        dataField: 'DE_ITEM',
        text: 'Product Name'
      },
      {
        dataField: 'DE_CATE',
        text: 'Category'
      },
      {
        dataField: 'DE_EQUI',
        text: 'Brand'
      },
      {
        dataField: 'DE_FAMI',
        text: 'Family'
      }];

    if(this.props.data.length === 0){ //If user didn't make a search -> redirect to /search
      return (<Redirect to="/search"/>);
    }
    return(
      <div className="box container">

        {/*Back button*/}
        <Button bsStyle="primary" className="btt-back" onClick={this.props.goBack} >
          <span className="glyphicon glyphicon-arrow-left" />
          {` Back`}
        </Button>

        {/*Number of rows*/}
        <span style={{"fontWeight":"bold"}}>
          {` Number of rows returned: ${this.props.data.length}`}</span>

        {/*Products table*/}
        <BootstrapTable
          keyField='ID_ITEM'
          data={ this.props.data }
          columns={ columns }
          pagination={ paginationFactory() } />

      </div>
    );
  }
}
export default Results;