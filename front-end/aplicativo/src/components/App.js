import React, { Component } from 'react';
import SearchForm from "./SearchForm";
import {Redirect, BrowserRouter,Switch,Route} from 'react-router-dom';
import '../style/app.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';

class App extends Component{

  render(){
    return(
      <BrowserRouter>
        <Switch>
          <Route path="/search" component={SearchForm} />
          <Route exact path="/" render={() => <Redirect to="/search"/> } />
          <Route path="*" render={() => <h3>Woops. Looks like this page doesn't exist.</h3>}/>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;