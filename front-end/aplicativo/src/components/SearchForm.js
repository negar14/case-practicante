import React, { Component } from 'react';
import {Form} from 'react-bootstrap';
import {Route,Link} from 'react-router-dom';
import axios from 'axios';
import util from '../resources/mylibrary';
import Results from "./Results";
import SelectControl from "./SelectControl";

class SearchForm extends Component{

  constructor(props){
    super(props);

    this.infoStructure = {};
    this.prodArray= {};

    this.state = {
      categoryList: [], //List of categories in the SelectControl component
      brandList: [], //List of brands in the SelectControl component
      familyList: [], //List of families in the SelectControl component

      valueCategory:"empty", //Current value of category
      valueBrand:"empty",  //Current value of brand
      valueFamily:"empty",  //Current value of family

      disableBrand: true, //SelectControl component disabled until the user choose a category
      disableFamily: true, //SelectControl component disabled until the user choose a brand

      datafiltered:[], //List of products that match all filters
    }

  }

  componentDidMount(){
    axios.get('http://200.16.7.151:3030/products')
      .then(response =>{
        this.prodArray = response.data;
        this.infoStructure = util.processInformation(response.data);
        this.setState({
          categoryList: util.getCategories(this.infoStructure)
        });
    }).catch(error =>{
        console.log(error);
    });
  }


  changeCatergory = e =>{
    this.setState({
      brandList: e.target.value ==="empty"? [] : util.getBrands(this.infoStructure,e.target.value),
      familyList: [],
      valueCategory: e.target.value,
      valueBrand: "empty",
      valueFamily: "empty",
      disableBrand: e.target.value==="empty",
      disableFamily: true,
    });
  };

  changeBrand = e =>{
    this.setState({
      familyList: e.target.value==="empty"?[] : util.getFamilies(this.infoStructure,this.state.valueCategory,e.target.value),
      valueBrand: e.target.value,
      valueFamily: "empty",
      disableFamily: e.target.value==="empty",
    });
  };

  changeFamily = e =>{
    this.setState({
      valueFamily: e.target.value
    });
  };

  submitForm = (e) =>{

    if(this.state.valueCategory !=="empty" &&

      this.state.valueFamily !=="empty" &&
      this.state.valueBrand !=="empty"){
      let results = util.getProducts(this.prodArray,this.infoStructure,this.state.valueCategory,this.state.valueBrand,this.state.valueFamily);
      this.setState({datafiltered: results})

    }else{

      e.preventDefault();
      alert("Please, fill all the fields");

    }
  };

  render(){
    return(
      <div>
        <Route exact path={this.props.match.path} render={()=>
          <div className="form-container">
            <h1 className="header">PRODUCT SEARCH</h1>
            <Form className="box">

              {/*CATEGORY SELECT*/}
              <SelectControl controlId="formCategory"
                             title="Category"
                             disabled={false}
                             value={this.state.valueCategory}
                             handleChange={this.changeCatergory}
                             defaultOption="Choose a category..."
                             list={this.state.categoryList}
              />

              {/*BRAND SELECT*/}
              <SelectControl controlId="formBrand"
                             title="Brand"
                             disabled={this.state.disableBrand}
                             value={this.state.valueBrand}
                             handleChange={this.changeBrand}
                             defaultOption="Choose a brand..."
                             list={this.state.brandList}
              />

              {/*FAMILY SELECT*/}
              <SelectControl controlId="formFamily"
                             title="Family"
                             disabled={this.state.disableFamily}
                             value={this.state.valueFamily}
                             handleChange={this.changeFamily}
                             defaultOption="Choose a family..."
                             list={this.state.familyList}
              />

              {/*SEARCH BUTTON*/}
              <Link to={`${this.props.match.url}/results`}
                    onClick={this.submitForm}
                    className="btn btn-primary btn-large centerButton">
                {`Find Products  `}<span className="glyphicon glyphicon-search"/>
              </Link>

            </Form>
          </div>
        }/>
        <Route path={`${this.props.match.path}/results`} render={ () =>
          <Results data={this.state.datafiltered}
                   goBack={this.props.history.goBack}
          />
        }/>
      </div>
    );
  }
}

export default SearchForm;