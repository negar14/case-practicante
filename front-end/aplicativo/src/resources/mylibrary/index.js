const funcs = {
  processInformation(prodArray){
    let tree = {};

    prodArray.forEach((item, index) => {
      if (item.DE_CATE in tree) {
        //Category already registered

        if (item.DE_EQUI in tree[item.DE_CATE]) {
          //Brand already registered in the category

          if (item.DE_FAMI in tree[item.DE_CATE][item.DE_EQUI]) {
            //Family aready registered in the brand
            tree[item.DE_CATE][item.DE_EQUI][item.DE_FAMI].push(index);
          } else {
            //New family
            tree[item.DE_CATE][item.DE_EQUI][item.DE_FAMI] = [index];
          }

        } else {
          //New Brand in the category
          tree[item.DE_CATE][item.DE_EQUI] = {};
          tree[item.DE_CATE][item.DE_EQUI][item.DE_FAMI] = [index];
        }

      } else {
        //New category
        tree[item.DE_CATE] = {};
        tree[item.DE_CATE][item.DE_EQUI] = {};
        tree[item.DE_CATE][item.DE_EQUI][item.DE_FAMI] = [index];
      }
    });
    return tree;
  },

  getCategories(data) {
    return Object.keys(data);
  },

  getBrands(data, category) {
    return Object.keys(data[category]);
  },

  getFamilies(data, category, brand) {
    return Object.keys(data[category][brand]);
  },
  getProducts(prodArray,data,category,brand,family){
    let result = [];
    data[category][brand][family].forEach((i) => {
      result.push(prodArray[i]);
    });
    return result;
  }
};

export default funcs;