
const express = require('express');
const router = express.Router();
const generalController = require('../controller/general');

router.get('/', function(req, res) {
    let information ={};
    information = generalController.getProductList();
    res.status(200).send(information);
});

module.exports = router;